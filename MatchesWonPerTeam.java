import java.io.BufferedReader;
import java.io.FileReader;
// import java.nio.Buffer;
import java.util.HashMap;
import java.io.*;

import javax.xml.crypto.Data;

// 2. Number of matches won of all teams over all the years of IPL.
public class MatchesWonPerTeam {
    {
        try {
            String path = "./matches.csv";
            BufferedReader matches = new BufferedReader(new FileReader(path));

            HashMap<String, Integer> innerMap = new HashMap<>();
            HashMap<String, HashMap<String, Integer>> matchesWonPerTeam = new HashMap<String, HashMap<String, Integer>>();

            String line;
            int countWinner = 0;
            matches.readLine();

            FileWriter newFile = new FileWriter("./Output/MatchesWonPerTeam.txt");

            while ((line = matches.readLine()) != null) {
                String[] data = line.split(",");
                String season = data[1];
                String winner = data[10];

                // innerMap.put(data[1], data[2]);
                // matchesWonPerTeam.put(data[0],innerMap);
                // System.out.println(matchesWonPerTeam);
                if (matchesWonPerTeam.containsKey(winner)) {
                    if (innerMap.containsKey(season)) {
                        countWinner += 1;
                        innerMap.put(season, countWinner);
                    } else {

                        innerMap.put(season, countWinner = +1);
                    }
                    matchesWonPerTeam.put(winner, innerMap);
                } else {
                    innerMap.put(season, countWinner = 1);
                    matchesWonPerTeam.put(winner, innerMap);
                }

            }
            newFile.write(matchesWonPerTeam.toString());
            // System.out.println(matchesWonPerTeam);
        } catch (Exception e) {
            // TODO: handle exception
            System.out.println(e);
        }

    }
}
