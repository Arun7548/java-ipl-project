// import javax.swing.plaf.synth.SynthEditorPaneUI;

// import jdk.javadoc.internal.doclets.formats.html.SourceToHTMLConverter;
import java.util.HashMap;
import java.util.Map;
import java.io.*;
import java.util.*;


// 1. Number of matches played per year of all the years in IPL.
public class MatchesPlayedPerYear {
    {

        try {
            String path = "./matches.csv";

            BufferedReader matches = new BufferedReader(new FileReader(path));

            System.out.println(matches.readLine());

            File  newFile = new File("./Output/MatchesPlayedPerYear.txt");
            int countMatch = 0;
            String line;
            HashMap<String, Integer> season = new HashMap<>();
            BufferedWriter outputFile = null;
            outputFile = new BufferedWriter(new FileWriter(newFile));

            while ((line = matches.readLine()) != null) {
                String[] data = line.split(",");

                if (season.containsKey(data[1])) {

                    season.put(data[1], countMatch += 1);

                    // p.put(key, map.getKey()+1);

                } else {
                    countMatch = 1;
                    season.put(data[1], countMatch);
                }

            }

            Iterator seasonIterator = season.entrySet().iterator();
            // HashMap<String,Integer> entery = new HashMap<>();
           while(seasonIterator.hasNext()){
               outputFile.write(seasonIterator.next().toString());
           }
            // newFile.write(season.toString());
            // System.out.println(season);


        } catch (Exception e) {
            // TODO: handle exception
            System.out.println(e);
        }

    }

}
